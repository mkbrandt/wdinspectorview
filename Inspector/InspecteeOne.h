//
//  InspecteeOne.h
//  Inspector
//
//  Created by Matt Brandt on 2/26/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WDInspectorView.h"

@interface InspecteeOne : NSObject <WDInspectableObject>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double weight;
@property (nonatomic, strong) NSString *bestFriend;

@end
