//
//  InspectorView.m
//  Inspector
//
//  Created by Matt Brandt on 2/26/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import "WDInspectorView.h"

@implementation WDInspectorView

- (void)closePreviousInspection
{
    if( self.myConstraints != nil )
        [self removeConstraints: self.myConstraints];
    self.myConstraints = nil;
    
    if( self.fields )
    {
        for( NSTextField *field in self.fields )
            [field removeFromSuperview];
        for( NSTextField *label in self.labels )
            [label removeFromSuperview];
    }
    
    self.fields = nil;
    self.labels = nil;
    
    if( [self.inspectee respondsToSelector: @selector(inspectionEnds:)] )
        [self.inspectee inspectionEnds: self];
}

- (void)controlTextDidChange: (NSNotification *)obj
{
    if( [self.delegate respondsToSelector: @selector(inspectorDidChangeValue:)] )
        [self.delegate inspectorDidChangeValue: self];
}

- (void)setInspectee: (id<WDInspectableObject>)inspectee
{
    [self closePreviousInspection];

    _inspectee = inspectee;
    if( inspectee == nil )
        return;

    self.keys = [inspectee inspectableKeys];
    if( [inspectee respondsToSelector: @selector(displayNames)] )
        self.displayNames = inspectee.displayNames;
    else
        self.displayNames = self.keys;

    self.fields = [NSMutableArray array];
    self.labels = [NSMutableArray array];
    
    NSTextField *title = [[NSTextField alloc] initWithFrame: NSMakeRect(0, 0, 0, 0)];
    NSDictionary *viewNames = NSDictionaryOfVariableBindings(title);
    title.stringValue = [NSString stringWithFormat: @"%@ Properties", inspectee.inspectionName];
    [title setBordered: NO];
    [title setEditable: NO];
    [title setSelectable: NO];
    title.alignment = NSCenterTextAlignment;
    [self addSubview: title];
    [title setTranslatesAutoresizingMaskIntoConstraints: NO];
    NSArray *titleConstraints = [NSLayoutConstraint constraintsWithVisualFormat: @"V:|-10-[title]"
                                                                        options: 0
                                                                        metrics: nil
                                                                          views: viewNames];
    [self.myConstraints addObject: titleConstraints];
    [self addConstraints: titleConstraints];
    
    titleConstraints = [NSLayoutConstraint constraintsWithVisualFormat: @"|-[title]-|"
                                                               options: 0
                                                               metrics: nil
                                                                 views: viewNames];
    [self.myConstraints addObject: titleConstraints];
    [self addConstraints: titleConstraints];
    
    NSTextField *previousField = title;
    for( int i = 0; i < self.keys.count; ++i )
    {
        NSString *key = self.keys[i];
        NSString *displayName = self.displayNames[i];
        NSTextField *label = [[NSTextField alloc] initWithFrame: NSMakeRect(0,0,0,0)];
        NSTextField *field = [[NSTextField alloc] initWithFrame: NSMakeRect(0,0,0,0)];
        
        [self.fields addObject: field];
        [self.labels addObject: label];
        
        label.stringValue = displayName;
        [label setBordered: NO];
        [label setEditable: NO];
        [label setSelectable: NO];
        
        id value = [inspectee valueForKey: key];
        [field setObjectValue: value];
        [field setBordered: YES];
        [field setEditable: YES];
        [field setSelectable: YES];
        [field setDelegate: self];
        [field bind: @"value" toObject: inspectee withKeyPath: key options: @{NSContinuouslyUpdatesValueBindingOption: @YES}];
        
        [self addSubview: label];
        [self addSubview: field];
        
        [label setTranslatesAutoresizingMaskIntoConstraints: NO];
        [field setTranslatesAutoresizingMaskIntoConstraints: NO];
        
        viewNames = NSDictionaryOfVariableBindings(previousField, label, field);
        NSArray *baseline = [NSLayoutConstraint constraintsWithVisualFormat: @"|-[label]-[field]-|"
                                                                            options: 0
                                                                            metrics: nil
                                                                            views: viewNames];
        [self addConstraints: baseline];
        [self.myConstraints addObjectsFromArray: baseline];
        
        if( previousField != title )
        {
            baseline = [NSLayoutConstraint constraintsWithVisualFormat: @"[field(==previousField)]-|"
                                                               options: 0
                                                               metrics: nil
                                                                 views: viewNames];
            [self addConstraints: baseline];
            [self.myConstraints addObjectsFromArray: baseline];
        }
        
        NSArray *vertical = [NSLayoutConstraint constraintsWithVisualFormat: @"V:[previousField]-[label]"
                                                                    options: 0
                                                                    metrics: nil
                                                                      views: viewNames];
        [self addConstraints: vertical];
        [self.myConstraints addObjectsFromArray: vertical];
        
        vertical = [NSLayoutConstraint constraintsWithVisualFormat: @"V:[previousField]-[field]"
                                                           options: 0
                                                           metrics: nil
                                                             views: viewNames];
        [self addConstraints: vertical];
        [self.myConstraints addObjectsFromArray: vertical];
        previousField.nextKeyView = field;
        previousField = field;
    }
    
    NSArray *bottom = [NSLayoutConstraint constraintsWithVisualFormat: @"V:[previousField]-50-|"
                                                       options: 0
                                                       metrics: nil
                                                         views: viewNames];
    [self addConstraints: bottom];
    [self.myConstraints addObjectsFromArray: bottom];
    previousField.nextKeyView = self.fields[0];
    [self prepareInspection];
}

- (void)drawRect:(NSRect)dirtyRect
{
    NSEraseRect(dirtyRect);
}

- (void)prepareInspection
{
    if( self.fields.count > 0 )
    {
        NSTextField *firstField = self.fields[0];
        
        [firstField selectText: self];
    }
}

@end
