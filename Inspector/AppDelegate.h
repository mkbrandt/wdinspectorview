//
//  AppDelegate.h
//  Inspector
//
//  Created by Matt Brandt on 2/26/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WDInspectorView.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet WDInspectorView *inspector;

@end
