//
//  InspecteeOne.m
//  Inspector
//
//  Created by Matt Brandt on 2/26/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import "InspecteeOne.h"

@implementation InspecteeOne

- (NSString *)inspectionName
{
    return @"MonkeyShiner";
}

- (NSArray *)inspectableKeys
{
    return @[@"name", @"weight", @"bestFriend"];
}

- (NSArray *)displayNames
{
    return @[@"Name", @"Weight", @"Best Friend"];
}

@end
