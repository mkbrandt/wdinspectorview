//
//  AppDelegate.m
//  Inspector
//
//  Created by Matt Brandt on 2/26/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import "AppDelegate.h"
#import "InspecteeOne.h"

@implementation AppDelegate
{
    InspecteeOne *i1, *i2;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    i1 = [[InspecteeOne alloc] init];
    i1.name = @"fred";
    i1.weight = 220.7;
    i1.bestFriend = @"Barney";
    
    i2 = [[InspecteeOne alloc] init];
    i2.name = @"Wilma";
    i2.weight = 112.3;
    i2.bestFriend = @"Betty";
    
    self.inspector.inspectee = i1;
}

- (IBAction)setInspectable1:(id)sender
{
    self.inspector.inspectee = i1;
}

- (IBAction)setInspectable2:(id)sender
{
    self.inspector.inspectee = i2;
}

@end
