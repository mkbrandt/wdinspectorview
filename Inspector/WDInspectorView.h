//
//  InspectorView.h
//  Inspector
//
//  Created by Matt Brandt on 2/26/14.
//  Copyright (c) 2014 Matt Brandt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef enum { IT_STRING, IT_INTEGER, IT_FLOAT, IT_BOOL } InspectableType;

@class WDInspectorView;

@protocol WDInspectableObject <NSObject>

@required

- (NSString *)inspectionName;
- (NSArray *)inspectableKeys;
- (id)valueForKey: (NSString *)key;
- (void)setValue: (id)value forKey:(NSString *)key;

@optional

- (NSArray *)displayNames;
- inspectionBegins: (WDInspectorView *)inspector;
- inspectionEnds: (WDInspectorView *)inspector;

@end

@protocol WDInspectorDelegate <NSObject>

- (void)inspectorDidChangeValue: (WDInspectorView *)inspector;

@end

@interface WDInspectorView : NSView <NSTextFieldDelegate>

@property (nonatomic, strong) NSArray *keys;
@property (nonatomic, strong) NSArray *displayNames;
@property (nonatomic, strong) NSMutableArray *fields;
@property (nonatomic, strong) NSMutableArray *labels;
@property (nonatomic, strong) NSMutableArray *myConstraints;
@property (nonatomic, strong) id<WDInspectableObject> inspectee;
@property (nonatomic, strong) IBOutlet id<WDInspectorDelegate> delegate;

- (void)prepareInspection;

@end


